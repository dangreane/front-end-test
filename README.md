#Front End Developer Exercise

## About
A broken Express, Require js site for FE candidates to fix and improve on

## Author 
Steve Zsolnai <steve@zolla.co.uk>

## Contributors
Daniel Greane <dan.greane@chelsea-apps.com>

## Run instructions:
1. `npm install grunt grunt-cli`
2. Install local dependencies `npm install`, `bower install`
3. run `grunt`
4. Run on to port http://localhost:3000. If you wish to use the Livereload plugin, it is included in the grunt task.
5. Fix the errors described on the page. 