
define([
  'jquery',
  'hbs!templates/searchResults'
],
function($, resultsTmpl) {
  'use strict';
  var $resultsWrapper = $('.results'),
      showResults = function(data) {
        $resultsWrapper.append(resultsTmpl(data));
      },
      loadResults = function(url) {
        $.ajax({
          url: url,
          type: 'GET',
          dataType: 'json',
        })
        .done(function(data) {
          showResults(data);
        })
        .fail(function() {
          console.log('error loading results');
        })
        .always(function() {
          console.log('complete');
        });
        
      },
      attachHandlers = function() {
        $('.js-loadResults').on('click', function(e) {
          e.preventDefault();
          loadResults($(this).attr('url'));
        });
      },
      init = function() {
        attachHandlers();
      };
  
  var Search = function() {

  };

  Search.init = function() {
    init();
  };

  return new Search();

});
