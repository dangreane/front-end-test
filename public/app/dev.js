
/* jshint laxcomma : true, quotmark:false */
/* global define */
define([
  'jquery',
  'components/jquery-mockjax/jquery.mockjax'
], function($) {
  'use strict';
  var Dev = function() {
    $.mockjax({
      url: '/results-api',
      responseTime: 200,
      proxy: '/data/results.json'
    });
  };

  return new Dev();
});