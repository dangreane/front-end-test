
define([
  'jquery',
  'Observable'
], function($, Observable) {
  'use strict';
  var timer,
      loadTime = new Date(),
      $timeNow = $('.js-loadTime'),
      init = function() {
        $timeNow.html($.timeago(loadTime));
        timer = setInterval(function() {
          $timeNow.html($.timeago(loadTime));
        }, 1000);

        $('.js-triggerFlashBox').on('click', function(e) {
          this.fire('SHOWFLASHBOX');
        }.bind(this));
      };

  
  var Ui = function() {
    Observable.call(this);
    init.call(this);
  };
  Ui.prototype = Observable.prototype;

  Ui.prototype.fire = function(evt) {
    $('body').addClass('isOnFire');
  }

  return Ui;

});
