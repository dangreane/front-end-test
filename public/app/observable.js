//
// Adapted by Chris Francis from the following original article:
// - http://www.nczonline.net/blog/2010/03/09/custom-events-in-javascript/
//
// Original attribution:
//Copyright (c) 2010 Nicholas C. Zakas. All rights reserved.
//MIT License

/*jslint browser: true, vars: true, white: true, forin: true, plusplus: true */
/*global define,window */

(function() {
  'use strict';
  define([], function () {
    function Observable() {
      this.obs = {};
    }
    Observable.prototype = {
      // Register a new subscriber for a certain event
      on: function(eventType, observer){
        if (typeof this.obs[eventType] === 'undefined'){
          this.obs[eventType] = [];
        }
        this.obs[eventType].push(observer);
      },
      // Notify all subscribers to a certain event
      fire: function(eventType, eventArgs){
        var observers,
          i,
          len;
        if (this.obs[eventType] instanceof Array) {
          observers = this.obs[eventType];
          for (i = 0, len = observers.length; i < len; i++) {
            observers[i].call(this, eventArgs);
          }
        }
      },
      // Remove a subscriber for a certain event 
      unsubscribe: function(eventType, observer){
        var observers,
          i,
          len;
        if (this.obs[eventType] instanceof Array){
          observers = this.obs[eventType];
          for (i = 0, len = observers.length; i < len; i++){
            if (observers[i] === observer){
              observers.splice(i, 1);
              break;
            }
          }
        }
      }
    };
    
    return Observable;
  });
}());